import React, { Component } from 'react';
import axios from "axios";
import { Switch, Link, Route } from "react-router-dom";
import './welove.css';
import Webdesign from './Webdesign';
import Branding from './Branding';
import Illustration from './Illustration';
import Animation from './Animation';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';





class Welove extends Component {

    constructor() {
        super();

        this.state = {
            sample: []
        }
    }
    componentDidMount() {
        axios.get("https://welovedaily.uk/an").then((dataObj) => {
            
            console.log(dataObj);
            this.setState({ sample: dataObj.data });
        })
       }
      
       
    
    
    
    
    render(){
        return <div className ="parent">
        <div className="submit" > <a  className="a"href={"https://welovedaily.com/submit/"} target={"_blank"}>submit   <span className="arrow">&#8594;</span></a></div>
          <h4 className="heart">we &#9825;</h4>
             <h1 className="head">Daily inspiration, right in your face.</h1>
            <h3>Tags:</h3>
            <div className="button"> 
                
              
                <Button className="asc" variant="contained" color="primary"  ><Link to="/">Webdesign</Link></Button>
                <Button  className="asc"variant="contained" color="primary" ><Link to="/br">Branding</Link></Button>
                <Button className="asc"variant="contained" color="primary" ><Link to="/il">Illustration</Link></Button>
                <Button className="asc"variant="contained" color="primary" ><Link to="/an">Animation</Link></Button>
            </div>
            <Switch>

            <Route exact path="/" component={Webdesign}/>
            <Route path="/br" component={Branding}/>
            <Route path="/il" component={Illustration}/>
            <Route path="/an" component={Animation}/>
            
        </Switch>
        
      
           
         

        </div>
        
        
        
    }
}
export default Welove;
