import React, { Component } from 'react';
import axios from "axios";
import './welove.css';

class Animation extends Component {

    constructor() {
        super();

        this.state = {
            sample: []
        }
    }
        componentDidMount() {
            axios.get("https://welovedaily.uk/an").then((dataObj) => {
                
                console.log(dataObj);
                this.setState({ sample: dataObj.data });
            })
           }
           render(){
            return <div>
            {
                this.state.sample.length > 0 ?
                   this.state.sample.map((e) => {
                        return <div className="style">
                            <img className="image" src={e.img} alt={e.id} />
                      <div className="name"><span className="span2">by</span> <span className="span1">{e.artist}</span></div>
                      <a className="source" href={e.reff} target={"_blank"}>{e.source}</a>
                      </div>
                    }): <h3>loadong...</h3>
                   
                } 

            </div>

           }
          
    }
    export default Animation;